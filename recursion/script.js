function factorial(n) {
  if (n == 1) {
    return n;
  }
  return n * factorial(n - 1);
}
console.log(factorial(4));

//Only takes in an array => split("")
function palindrome(s) {
  if (s.length <= 1) {
    return true;
  } else {
    if (s.pop() == s.shift() && palindrome(s)) {
      return true;
    }
  }
}
console.log(palindrome("asdfdsa".split("")));

function binarySearch(arr, target, low, high) {
  if (low > high) {
    return -1;
  }
  var mid = Math.floor((low + high) / 2);

  if (arr[mid] == target) {
    return mid;
  }
  if (target < arr[mid]) {
    return binarySearch(arr, target, low, mid - 1);
  } else {
    return binarySearch(arr, target, mid + 1, high);
  }
}
var arr = [1, 33, 55, 66, 88, 99, 121, 141, 166, 245];
var target = 121;

console.log(binarySearch(arr, target, 0, arr.length - 1));

var arr2 = [1, 33, 55, 66, 88, 99, 121, 141, 166, 245];
var target2 = 121;

function linearSearch(a, t, n) {
  if (n > a.length) {
    return false;
  } else if (a[n] == t) {
    return n;
  } else {
    return linearSearch(a, t, n + 1);
  }
}

console.log(linearSearch(arr2, target2, 0));

function addition_i_with_n(n) {
  if (n == 1) {
    return 2;
  } else {
    return n + addition_i_with_n(n - 1);
  }
}

console.log(addition_i_with_n(6));

function setup() {}

function draw() {}
