//for each of these graphs use http://graphonline.ru/en/
//to draw them
//
//then write a breadth first and depth first order of nodes
//starting from node A
//there are usually more than 1 different breadth or depth first
//traveral orders for a graph

var graphA = {

  "A":["B"],
  "B":["A","C","D"],
  "C":["B","D"],
  "D":["B","C","E"],
  "E":["D","F"],
  "F":["E"]

}

var graphB = {

  "A":["B","C"],
  "B":["A","D","E"],
  "C":["A"],
  "D":["B","F","G"],
  "E":["B"],
  "F":["D","H"],
  "G":["D"],
  "H":["F"],
}

var graphC = {

  "A":["B","F"],
  "B":["A","F","E"],
  "C":["E","D"],
  "D":["C","E"],
  "E":["B","C","D"],
  "F":["A","B"]

}
var graphD = {

  "A":["B","C","E","F","X"],
  "B":["A","D","H"],
  "C":["A"],
  "D":["B"],
  "E":["A","I"],
  "F":["A","J"],
  "G":["X"],
  "H":["B","K","L"],
  "I":["E"],
  "J":["F","M","N"],
  "K":["H"],
  "L":["H"],
  "M":["J"],
  "N":["J"],
  "X":["A","G"]

}


function BFS(graph, startNode){
  var order = []
  var stack = []
  stack.push(startNode)
  while (stack.length>0){
    let currentNode = stack.shift()
    order.push(currentNode)
    let connectedNodes = graph[currentNode]
    for(let node of connectedNodes){
      if (!(order.includes(node) || stack.includes(node))){
        stack.push(node)
      }
    }
  }
  return order
}

console.log(BFS(graphA,"A"))

function DFS(graph, startNode){
var order = []
  var stack = []
  stack.push(startNode)
  while (stack.length>0){
    let currentNode = stack.pop()
    order.push(currentNode)
    let connectedNodes = graph[currentNode]
    for(let node of connectedNodes){
      if (!(order.includes(node) || stack.includes(node))){
        stack.push(node)
      }
    }
  }
  return order
}

console.log(DFS(graphA,"A"))
