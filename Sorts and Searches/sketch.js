var a = [9, 3, 6, 4, 2, 7, 5, 0, 3, 8, 1];

function bubbleSort(x) {
  var count = 0;
  do {
    var swap = false;
    for (var i = 0; i < a.length - count; i++) {
      if (x[i] < x[i - 1]) {
        temp = x[i];
        x[i] = x[i - 1];
        x[i - 1] = temp;
      }
      swap = true;
    }
    count++;
  } while (swap);
  return x;
}

function insertionSort(x) {
  for (var i = 0; i < x.length; i++) {
    var a = i;
    var temp = x[a];
    while (temp < x[a - 1] && a > 0) {
      x[a] = x[a - 1];
      a--;
    }
    x[a] = temp;
  }
  return x;
}

console.log(insertionSort(a));
