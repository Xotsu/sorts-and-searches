function setup() { }

function draw() { }

function mergeSort(arr) {
    if (arr.length <= 1) {
        return arr
    }
    var middle = Math.floor(arr.length / 2)
    var left = arr.slice(0, middle)
    var right = arr.slice(middle, arr.length)
    return merge(mergeSort(left), mergeSort(right))
}

function merge(left, right) {
    var arr = []
    while (left.length > 0 || right.length > 0) {

        if (left[0] > right[0] || left.length === 0) {
            arr.push(right.shift())
        } else {
            arr.push(left.shift())
        }
    }
    return arr
}

var arr1 = [0, 2, 6, 3, 7, 3, 2]
var arr2 = [12, 432, 654, 1, 2, 0, 5]
var arr3 = [324, 346, 3, 1, 754, 234, 8, 234, 8, 67, 345, 2, 37, 56, 2, 34, 856, 2136343, 213]

console.log(mergeSort(arr3))