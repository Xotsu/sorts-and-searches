function setup() { }

function draw() { }

var arr = [1, 8, 5, 234, 543, 47, 345, 23, 856, 2, 120]

console.log(arr)
quicksort(0, arr.length - 1)
console.log(arr)

function quicksort(low, piv) {
  if (low < piv) {
    var pi = partition(low, piv)
    quicksort(low, pi - 1)
    quicksort(pi + 1, piv)
  }
}

function partition(low, piv) {
  do {
    if (arr[low] < arr[piv] && low < piv) {
      low++
    } else if (arr[low] > arr[piv] && low < piv) {
      let temp = arr[low]
      arr[low] = arr[piv]
      arr[piv] = temp
      temp = low
      low = piv
      piv = temp
      low--
    } else if (arr[low] < arr[piv] && low > piv) {
      let temp = arr[low]
      arr[low] = arr[piv]
      arr[piv] = temp
      temp = low
      low = piv
      piv = temp
      low++
    } else {
      low--
    }
  } while (low != piv)
  return (low)
}