class Node {
  constructor(data) {
    this.data = data;
    this.pointsTo = null;
  }
}

class LList {
  constructor() {
    this.head = null;
  }
  prepend(item) {
    var nodeToAdd = new Node(item);
    nodeToAdd.pointsTo = this.head;
    this.head = nodeToAdd;
  }
  print() {
    var current = this.head;
    while (current != null) {
      console.log(current.data);
      current = current.pointsTo;
    }
    console.log("");
  }
  append(item) {
    var nodeToAdd = new Node(item);
    if (this.head == null) {
      this.head = nodeToAdd;
      return;
    } else {
      var current = this.head;
      while (current.pointsTo != null) {
        current = current.pointsTo;
      }
      current.pointsTo = nodeToAdd;
    }
  }
  addInOrder(item) {
    var nodeToAdd = new Node(item);
    if (this.head == null || item < this.head.data) {
      nodeToAdd.pointsTo = this.head;
      this.head = nodeToAdd;
    } else {
      var current = this.head;
      while (current.pointsTo != null && item > current.pointsTo.data) {
        current = current.pointsTo;
      }
      nodeToAdd.pointsTo = current.pointsTo;
      current.pointsTo = nodeToAdd;
    }
  }
  remove(item) {
    if (item == this.head.data) {
      this.head = this.head.pointsTo;
    } else {
      var current = this.head;
      while (current.pointsTo != null && item != current.pointsTo.data) {
        current = current.pointsTo;
      }
      current.pointsTo = current.pointsTo.pointsTo;
    }
  }
}

var l = new LList();
l.prepend(1000);
l.prepend(500);
l.prepend(100);
l.print();
l.append(2000);
l.print();
l.addInOrder(50);
l.print();
l.addInOrder(750);
l.print();
l.remove(750);
l.print();
