function addName(nameToAdd) {
  if (nf == -1) {
    console.log("can't add as the list is full");
    return;
  }
  if (start == -1 || nameToAdd < names[start].name) {
    console.log("the new name will be the first one in the list");
    //code to insert as the first item in the list goes here
  } else {
    //this code works out which item to insert the new item after
    //make sure you understand how it works - ask me if not sure!
    var current = start;
    var previous = null;

    while (current != -1 && nameToAdd > names[current].name) {
      previous = current;
      current = names[current].pointer;
    }

    console.log("the new name will be inserted after " + names[previous].name);
    console.log("the index to insert after is " + previous);
    //code to insert into a list goes here
  }
}
