var names = [];
var nf;
var start;

class node {
  constructor() {
    this.name;
    this.pointer;
  }
}

createEmptyList();
showData();
addDataFromBook();
showData();
printItemsInOrder();

function printItemsInOrder() {
  //write code here to print out the
  //items in the list in alphabetical order
  var c = start;
  while (c != -1) {
    console.log(names[c].name);
    c = names[c].pointer;
  }
}

function createEmptyList() {
  nf = 0;
  start = -1;
  for (var i = 0; i <= 5; i++) {
    var item = new node();
    item.pointer = i + 1;
    names.push(item);
  }

  names[i - 1].pointer = -1;
}

function showData() {
  //function to log out the state of the names array
  //to the console so you can see what is going on
  for (var i = 0; i < names.length; i++) {
    console.log(i + " | " + names[i].name + " | " + names[i].pointer);
  }
  console.log("start = " + start + " and next free = " + nf);
  console.log("");
}

function addDataFromBook() {
  //this is just to add the data from the text book example p193
  //it is added directly just so you have some names to test your
  //printInOrder function (and your add/delete functions later)
  names[0].name = "Browning";
  names[0].pointer = 3;

  names[1].name = "Turner";
  names[1].pointer = -1;

  names[2].name = "Johnson";
  names[2].pointer = 1;

  names[3].name = "Cray";
  names[3].pointer = 2;
  nf = 4;
  start = 0;
}
